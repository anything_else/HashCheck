﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.Security.Authentication;
using System.Diagnostics;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace HashCheck
{
    public class Hasher
    {
#region Pbkfd2
        public static (byte[] hash, byte[] salt) HashPbkdf2(string toHash, byte [] salt = null)
        {
            salt = AddSpice(salt);

            return (KeyDerivation.Pbkdf2(toHash, salt, KeyDerivationPrf.HMACSHA512, 100000, 256/8), salt);
        }
#endregion

#region SHA256
        public static (byte[] hash, byte[] salt) HashData256Salted(byte[] data, byte[] salt = null)
        {
            salt = AddSpice(salt);

            var saltedData = new byte[salt.Length + data.Length];

            salt.CopyTo(saltedData, 0);
            data.CopyTo(saltedData, salt.Length);

            return (HashData256(saltedData), salt);
        }

        public static byte[] HashData256(byte[] data)
        {
            using(var hasher = new SHA256Managed())
            {
                return hasher.ComputeHash(data);
            }
        }

        public static (byte[] hash, byte[] salt, byte[] pepper) HashData256H(
            byte[] data, byte[] salt = null, byte[] pepper = null)
        {
            if(salt == null && pepper != null || salt != null && pepper == null)
            {
                throw new ArgumentNullException("One of spices missing");
            }

            salt = AddSpice(salt);
            pepper = AddSpice(pepper);

            var saltedData = new byte[salt.Length + data.Length + pepper.Length];
            
            salt.CopyTo(saltedData, 0);
            data.CopyTo(saltedData, salt.Length);
            
            using(var hasher = new HMACSHA256(pepper))
            {
                return (hasher.ComputeHash(saltedData), salt, pepper);
            }            
        }
#endregion

#region Sha512
        public static (byte[] hash, byte[] salt) HashData512Salted(byte[] data, byte[] salt = null)
        {
            salt = AddSpice(salt);

            var saltedData = new byte[salt.Length + data.Length];

            salt.CopyTo(saltedData, 0);
            data.CopyTo(saltedData, salt.Length);

            return (HashData512(saltedData), salt);
        }

        public static byte[] HashData512(byte[] data)
        {
            using(var hasher = new SHA512Managed())
            {
                return hasher.ComputeHash(data);
            }
        }

        public static (byte[] hash, byte[] salt, byte[] pepper) HashData512H(
            byte[] data, byte[] salt = null, byte[] pepper = null)
        {
            if(salt == null && pepper != null || salt != null && pepper == null)
            {
                throw new ArgumentNullException("One of spices missing");
            }

            salt = AddSpice(salt);
            pepper = AddSpice(pepper);

            var saltedData = new byte[salt.Length + data.Length + pepper.Length];
            
            salt.CopyTo(saltedData, 0);
            data.CopyTo(saltedData, salt.Length);
            
            using(var hasher = new HMACSHA512(pepper))
            {
                return (hasher.ComputeHash(saltedData), salt, pepper);
            }            
        }
#endregion

#region private handlers
        private static byte[] AddSpice(byte[] spice)
        {
            if(spice == null) 
            {
                spice = new byte[256/8];
                using(var saltProvider = new RNGCryptoServiceProvider())
                {
                    saltProvider.GetBytes(spice);
                }
            }

            return spice;
        }
#endregion
    }
}
