using System;
using Xunit;
using HashCheck;

namespace test
{
    public class Pbkdf2Test
    {
        [Fact]
        public void Test_HahsPbkdf2()
        {
            var t =  "dupa jasiu karuzela";
            var one = Hasher.HashPbkdf2(t);
            var two = Hasher.HashPbkdf2(t, one.salt);

            Assert.Equal(Convert.ToBase64String(one.salt), Convert.ToBase64String(two.salt));
            Assert.Equal(Convert.ToBase64String(one.hash), Convert.ToBase64String(two.hash));
        }
        
    }
}