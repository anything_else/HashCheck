using System;
using Xunit;
using HashCheck;

namespace test
{
    public class Sha256Test
    {
        [Fact]
        public void Test_SHA256Salted()
        {
            var t =  "dupa jasiu karuzela";
            var x = System.Text.Encoding.UTF8.GetBytes(t);
            var one = Hasher.HashData256Salted(x);
            var two = Hasher.HashData256Salted(x, one.salt);

            Assert.Equal(Convert.ToBase64String(one.salt), Convert.ToBase64String(two.salt));
            Assert.Equal(Convert.ToBase64String(one.hash), Convert.ToBase64String(two.hash));
        }

        [Fact]
        public void Test_HMACSHA256()
        {
            var t =  "dupa jasiu karuzela";
            var x = System.Text.Encoding.UTF8.GetBytes(t);
            var one = Hasher.HashData256H(x);
            var two = Hasher.HashData256H(x, one.salt, one.pepper);

            Assert.Equal(Convert.ToBase64String(one.salt), Convert.ToBase64String(two.salt));
            Assert.Equal(Convert.ToBase64String(one.hash), Convert.ToBase64String(two.hash));
            Assert.Equal(Convert.ToBase64String(one.pepper), Convert.ToBase64String(two.pepper));
        }

        [Fact]
        public void Test_HMACSHA256_Error()
        {
            var t =  "dupa jasiu karuzela";
            var x = System.Text.Encoding.UTF8.GetBytes(t);
            var one = Hasher.HashData256H(x);
            
            Assert.Throws<ArgumentNullException>(() => Hasher.HashData256H(x, one.salt));
        }
    }
}