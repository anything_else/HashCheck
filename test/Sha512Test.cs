using System;
using Xunit;
using HashCheck;

namespace test
{
    public class Sha512Test
    {
        [Fact]
        public void Test_SHA512Salted()
        {
            var t =  "dupa jasiu karuzela";
            var x = System.Text.Encoding.UTF8.GetBytes(t);
            var one = Hasher.HashData512Salted(x);
            var two = Hasher.HashData512Salted(x, one.salt);

            Assert.Equal(Convert.ToBase64String(one.salt), Convert.ToBase64String(two.salt));
            Assert.Equal(Convert.ToBase64String(one.hash), Convert.ToBase64String(two.hash));
        }

        [Fact]
        public void Test_HMACSHA512()
        {
            var t =  "dupa jasiu karuzela";
            var x = System.Text.Encoding.UTF8.GetBytes(t);
            var one = Hasher.HashData512H(x);
            var two = Hasher.HashData512H(x, one.salt, one.pepper);

            Assert.Equal(Convert.ToBase64String(one.salt), Convert.ToBase64String(two.salt));
            Assert.Equal(Convert.ToBase64String(one.hash), Convert.ToBase64String(two.hash));
            Assert.Equal(Convert.ToBase64String(one.pepper), Convert.ToBase64String(two.pepper));
        }

        [Fact]
        public void Test_HMACSHA512_Error()
        {
            var t =  "dupa jasiu karuzela";
            var x = System.Text.Encoding.UTF8.GetBytes(t);
            var one = Hasher.HashData256H(x);
            
            Assert.Throws<ArgumentNullException>(() => Hasher.HashData256H(x, one.salt));
        }
    }
}